#include "DatabaseAccess.h"
/*
int main(void)
{
	DatabaseAccess database;
	database.open();

	User user(100, "Yehuda");
	User user2(200, "Yehuda2");
	Album album(100, "myAlbum", "112232");
	Album album2(200, "myAlbum2", "1122321");
	Picture picture(1, "myPic", "Image/1.png", "3/11/2022");
	database.createUser(user);
	database.createUser(user2);
	database.createAlbum(album);
	database.createAlbum(album2);
	database.addPictureToAlbumByName("myAlbum", picture);
	database.tagUserInPicture("myAlbum", "myPic", 100);
	database.printAlbums();
	database.printUsers();
	std::list<Album> albumist = database.getAlbums();
	for (auto run = albumist.begin(); run != albumist.end(); run++)
	{
		std::cout << "\nid- " << run->getOwnerId() << "\nname- " << run->getName() << "\ndate- " << run->getCreationDate() << std::endl;
	}
	std::list<Album> albumist2 = database.getAlbumsOfUser(user);
	for (auto run = albumist2.begin(); run != albumist2.end(); run++)
	{
		std::cout << "\nid- " << run->getOwnerId() << "\nname- " << run->getName() << "\ndate- " << run->getCreationDate() << std::endl;
	}
	User user3 = database.getUser(200);
	std::cout << "\nID- " << user3.getId() << "\nName- " << user3.getName() << "\n";
	std::cout << database.doesUserExists(200) << "\n";   //should return true
	std::cout << database.doesUserExists(100) << "\n";   //should return true
	std::cout << database.doesUserExists(300) << "\n\n";   //should return false

	std::cout << database.doesAlbumExists("myAlbum", 100) << "\n";   //should return true
	std::cout << database.doesAlbumExists("myAlbum2", 200) << "\n";   //should return true
	std::cout << database.doesAlbumExists("myAlbum", 200) << "\n";   //should return false
	std::cout << database.doesAlbumExists("myAlbumm", 100) << "\n";   //should return false
	std::cout << database.doesAlbumExists("myAlbummm", 50) << "\n\n";   //should return false

	Album a1 = database.openAlbum("myAlbum");
	Album a2 = database.openAlbum("myAlbum2");
	Album a3 = database.openAlbum("myAlbum3");
	std::cout << "\nid- " << a1.getOwnerId() << "\nname- " << a1.getName() << "\ndate- " << a1.getCreationDate() << std::endl;
	std::cout << "\nid- " << a2.getOwnerId() << "\nname- " << a2.getName() << "\ndate- " << a2.getCreationDate() << std::endl;
	//std::cout << "\nid- " << a3.getOwnerId() << "\nname- " << a3.getName() << "\ndate- " << a3.getCreationDate() << std::endl;

	//database.untagUserInPicture("myAlbum", "myPic", 100);
	//database.removePictureFromAlbumByName("myAlbum", "myPic");
	//database.deleteUser(user);

	//todo- check the deleteAlbum

	return 0;
}
*/