#include "DatabaseAccess.h"

bool DatabaseAccess::open()
{
	string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		cout << "Failed to open DB" << endl;
		return -1;
	}
	if (doesFileExist == 0) {
		fprintf(stderr, "Opened database successfully\n");
	}
	else {

		const char* tables[4];
		//***create table commends***
		tables[0] = "CREATE TABLE USERS("  \
			"ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," \
			"NAME TEXT NOT NULL);";

		tables[1] = "CREATE TABLE ALBUMS("  \
			"ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," \
			"NAME TEXT NOT NULL,"\
			"CREATION_DATE TEXT NOT NULL,"\
			"USER_ID INTEGER NOT NULL,"\
			"FOREIGN KEY(USER_ID) REFERENCES USERS(ID));";

		tables[2] = "CREATE TABLE PICTURES("\
			"ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," \
			"NAME TEXT NOT NULL,"\
			"LOCATION TEXT NOT NULL,"\
			"CREATION_DATE TEXT NOT NULL,"\
			"ALBUM_ID TEXT NOT NULL,"\
			"FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));";
		tables[3] = "CREATE TABLE TAGS("\
			"ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," \
			"PICTURE_ID INTEGER NOT NULL,"\
			"USER_ID INTEGER NOT NULL,"\
			"FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID));";

		char* errMessage = nullptr;

		for (int i = 0; i < 4; i++) {
			res = sqlite3_exec(db, tables[i], nullptr, nullptr, &errMessage);
			if (res != SQLITE_OK)
				return false;
		}
	}
	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(db);
	db = nullptr;
}

void DatabaseAccess::clear()
{
}

int DatabaseAccess::getLastId(std::string tableName)
{
	std::string getLastId = "SELECT ID FROM " + tableName + " ORDER BY ID DESC LIMIT 1;";
	int id = -1;
	int res = sqlite3_exec(db, getLastId.c_str(), callback_lastId, (void*)(&id), &errMessage);
	if (id == -1)  //first user in table(table was empty before)
		id = 1;
	else
		id++;
	return id;
}

DatabaseAccess::~DatabaseAccess()
{
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> albumsList;
	char* print = "SELECT * FROM ALBUMS;";
	std::list<Album>* ptr = &albumsList;
	int res = sqlite3_exec(db, print, callback_getAlbumes, (void*)(&albumsList), &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error get albums!\n";
	}
	return albumsList;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albumsList;
	char* print = "SELECT * FROM ALBUMS;";
	Album album(user.getId(), "null", "null");     //I want to save userId for callback so I saving id in list for now
	albumsList.push_back(album);
	int res = sqlite3_exec(db, print, callback_getAlbumesOf, (void*)(&albumsList), &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error get albums!\n";
	}
	return albumsList;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	std::string pre = "INSERT INTO ALBUMS (NAME, CREATION_DATE, USER_ID) VALUES (";
	std::string post = ");";
	std::string createTable = pre + "'" + album.getName() + "', '" + album.getCreationDate() + "', " + std::to_string(album.getOwnerId()) + post;
	int res = sqlite3_exec(db, createTable.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error create this album!\m";
	}
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string pre = "DELETE FROM ALBUMS WHERE ID=";
	std::string deleteAlbum = pre + std::to_string(userId) + ";";
	int res = sqlite3_exec(db, deleteAlbum.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error create this user!\n";
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	char* getAllAlbums = "SELECT * FROM ALBUMS;";
	Album album(userId, albumName, "null");
	int res = sqlite3_exec(db, getAllAlbums, callback_albumExist, (void*)&album, &errMessage);
	if (album.getCreationDate() != "null")
		return true;
	return false;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	int res;
	char* getAllAlbums = "SELECT * FROM ALBUMS;";
	Album album(-999, albumName, "null");
	res = sqlite3_exec(db, getAllAlbums, callback_openAlbum, (void*)&album, &errMessage);
	
	std::list<Picture> lPics;
	string query = "SELECT * FROM PICTURES WHERE ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\");";	
	res = sqlite3_exec(db, query.c_str(), callback_getPictures, (void*)&lPics, &errMessage);
	for (auto it = lPics.begin(); it != lPics.end(); it++) {
		std::set<int> tagsInPic;
		std::string getTags = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID=(SELECT ID FROM PICTURES WHERE NAME= \"" + it->getName() + "\");";
		res = sqlite3_exec(db, getTags.c_str(), callback_getTags, (void*)&tagsInPic, &errMessage);
		for (auto i = tagsInPic.begin(); i != tagsInPic.end(); i++) {
			it->tagUser(*i);
		}
		album.addPicture(*it);
	}
	return album;
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	// basically here we would like to delete the allocated memory we got from openAlbum
}

void DatabaseAccess::printAlbums()
{
	char* print = "SELECT * FROM ALBUMS;";
	int res = sqlite3_exec(db, print, callback_printTable, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error print this album!\n";
	}
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	std::string pre = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES (";
	std::string post = ");";
	int res;

	//get album ID
	std::string getPicId = "SELECT ID FROM ALBUMS WHERE NAME=\"" + albumName + "\";";
	int id = -1;
	int* ptr = &id;
	res = sqlite3_exec(db, getPicId.c_str(), callback_getId, (void*)ptr, &errMessage);
	id = *ptr;
	//add picture
	std::string picData = "'" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', " + std::to_string(id);
	std::string addPic = pre + picData + post;
	res = sqlite3_exec(db, addPic.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error create this picture!\n";
	}
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string deletePic = "DELETE FROM PICTURES WHERE NAME=";
	deletePic += "\"" + pictureName + "\";";
	int res = sqlite3_exec(db, deletePic.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error delete this picture!\n";
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string pre = "INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES (";
	std::string post = ");";

	int res;
	//we should get the picture ID
	std::string getPicId = "SELECT ID FROM PICTURES WHERE NAME=\"" + pictureName + "\";";
	int id = -1;
	int* ptr = &id;
	res = sqlite3_exec(db, getPicId.c_str(), callback_getId, (void*)ptr, &errMessage);
	id = *ptr;
	std::string tagUser = pre + std::to_string(id) + ", " + std::to_string(userId) + post;
	
	res = sqlite3_exec(db, tagUser.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error tag!\n";
	}
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string pre = "DELETE FROM TAGS WHERE PICTURE_ID=";
	std::string getPicId = "SELECT ID FROM PICTURES WHERE NAME=\"" + pictureName + "\";";
	int res;

	//we should get the picture ID
	int id = -1;
	int* ptr = &id;
	res = sqlite3_exec(db, getPicId.c_str(), callback_getId, (void*)ptr, &errMessage);
	id = *ptr;
	std::string untagUser = pre + std::to_string(id) + ";";
	res = sqlite3_exec(db, untagUser.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error tag!\n";
	}
}

void DatabaseAccess::printUsers()
{
	char* print = "SELECT * FROM USERS;";
	int res = sqlite3_exec(db, print, callback_printTable, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error print this album!\n";
	}
}

User DatabaseAccess::getUser(int userId)
{
	char* getallUsers = "SELECT * FROM USERS;";
	User user(userId, "null");   //saving id for callback function
	int res = sqlite3_exec(db, getallUsers, callback_getUsers, (void*)&user, &errMessage);
	return user;
}

void DatabaseAccess::createUser(User& user)
{
	int id = getLastId("USERS");
	user.setId(id);
	std::string pre = "INSERT INTO USERS (ID, NAME) VALUES (";
	std::string post = ");";
	std::string createTable = pre + "'" + std::to_string(id) + "', '" + user.getName() + "'" + post;
	int res = sqlite3_exec(db, createTable.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error create this album!\n";
	}
}

void DatabaseAccess::deleteUser(const User& user)
{
	std::string pre = "DELETE FROM USERS WHERE ID=";
	std::string deleteUser = pre + std::to_string(user.getId()) + ";";
	int res = sqlite3_exec(db, deleteUser.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "error create this user!\n";
	}
}

bool DatabaseAccess::doesUserExists(int userId)
{
	//the same code like function 'getUser' because it's the same idea
	char* getallUsers = "SELECT * FROM USERS;";
	User user(userId, "null");   //saving id for callback function
	int res = sqlite3_exec(db, getallUsers, callback_getUsers, (void*)&user, &errMessage);
	if (user.getName() != "null")
		return true;
	return false;
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	std::string query = "SELECT * FROM ALBUMS WHERE USER_ID=\"" + (std::to_string)(user.getId()) + "\";";
	int countUsers = 0;
	int res = sqlite3_exec(db, query.c_str(), callback_coutUsers, (void*)&countUsers, &errMessage);
	return countUsers;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int count = 0;
	int res;
	std::list<Picture> lPics;
	string query = "SELECT * FROM PICTURES;";      //get all pictures
	res = sqlite3_exec(db, query.c_str(), callback_getPictures, (void*)&lPics, &errMessage);
	
	std::string query2 = "SELECT * FROM PICTURES;";    //add albumId for each picture with replacing creationDate with albumId
	res = sqlite3_exec(db, query2.c_str(), callback_creatinDate_toAlbumId, (void*)&lPics, &errMessage);
	
	for (auto it = lPics.begin(); it != lPics.end(); it++) {
		std::set<int> tagsInPic;
		std::string getTags = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID=(SELECT ID FROM PICTURES WHERE NAME= \"" + it->getName() + "\");";
		res = sqlite3_exec(db, getTags.c_str(), callback_getTags, (void*)&tagsInPic, &errMessage);    //get all user tagged in albums of user
		for (auto i = tagsInPic.begin(); i != tagsInPic.end(); i++) {
			it->tagUser(*i);
		}
	}

	for (auto it = lPics.begin(); it != lPics.end(); it++) {
		bool retVal = it->isUserTagged(user);
		if (retVal == 0) {
			//if user is not tagged in this pic so e dont need this picture
			it->setName("null");
		}
	}

	std::set<std::string> allAlbumsID;
	std::string query3 = "SELECT * FROM ALBUMS;";     //get all albums
	res = sqlite3_exec(db, query3.c_str(), callback_getAlbumesStr, (void*)&allAlbumsID, &errMessage);
	for (auto it = lPics.begin(); it != lPics.end(); it++) {
		if (allAlbumsID.find(it->getCreationDate()) != allAlbumsID.end() && it->getName() != "null") {
			allAlbumsID.erase(it->getCreationDate());     //delete when found to avoid duplicates
			count++;
		}
	}
	return count;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	int count = 0;
	int res;
	std::list<Picture> lPics;
	std::set<std::string> allAlbumsID;
	std::string query = "SELECT * FROM ALBUMS WHERE USER_ID=\"" + (std::to_string)(user.getId()) + "\";";    //get albumes of user
	res = sqlite3_exec(db, query.c_str(), callback_getAlbumesStr, (void*)&allAlbumsID, &errMessage);
	
	string query2 = "SELECT * FROM PICTURES;";    //get all pictures
	res = sqlite3_exec(db, query2.c_str(), callback_getPictures, (void*)&lPics, &errMessage);
	
	std::string query3 = "SELECT * FROM PICTURES;";   //add albumId for each picture with replacing creationDate with albumId
	res = sqlite3_exec(db, query3.c_str(), callback_creatinDate_toAlbumId, (void*)&lPics, &errMessage);
	
	for (auto it = lPics.begin(); it != lPics.end(); it++) {
		std::set<int> tagsInPic;
		std::string getTags = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID=(SELECT ID FROM PICTURES WHERE NAME= \"" + it->getName() + "\");";
		res = sqlite3_exec(db, getTags.c_str(), callback_getTags, (void*)&tagsInPic, &errMessage);    //get all user tagged in albums of user
		for (auto i = tagsInPic.begin(); i != tagsInPic.end(); i++) {
			it->tagUser(*i);
		}
	}
	for (auto it = lPics.begin(); it != lPics.end(); it++) {
		bool retVal = it->isUserTagged(user);
		if (retVal != 0) {
			count++;
		}
	}
	return count;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	return countTagsOfUser(user) / countAlbumsOwnedOfUser(user);
}

User DatabaseAccess::getTopTaggedUser()
{
	User user(-999, "null");
	char* query = "SELECT * FROM TAGS INNER JOIN USERS ORDER BY USER_ID ASC LIMIT 1;";
	int res = sqlite3_exec(db, query, callback_getUser, (void*)&user, &errMessage);
	return user;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	Picture picture(999, "999");
	char* query = "SELECT * FROM TAGS INNER JOIN PICTURES ORDER BY USER_ID ASC LIMIT 1;";
	int res = sqlite3_exec(db, query, callback_getPic, (void*)&picture, &errMessage);
	return picture;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::string query1 = "SELECT PICTURE_ID FROM TAGS WHERE USER_ID=" + std::to_string(user.getId()) + ";";
	std::string query2 = "SELECT * FROM PICTURES;";
	std::set<int> tags;    
	std::list<Picture> pics;
	std::list<Picture> newPics;
	int res;
	res = sqlite3_exec(db, query1.c_str(), callback_getTags, (void*)&tags, &errMessage);   //get all tags of this user
	res = sqlite3_exec(db, query2.c_str(), callback_getPictures, (void*)&pics, &errMessage);   //get all pictures
	for (auto it = pics.begin(); it != pics.end(); it++) {
		if (tags.find(it->getId()) != tags.end()) {
			Picture pic(it->getId(), it->getName());
			newPics.push_back(pic);
		}
	}
	return newPics;
}

int DatabaseAccess::editPicture(std::string path)
{
	STARTUPINFO info = { sizeof(info) };
	PROCESS_INFORMATION processInfo;
	std::cout << "Which app would you want? paint- 1, inferView- else\n";
	int mod;
	std::string app;
	std::cin >> mod;
	if(mod == 1)
		app = "C:\\Windows\\system32\\mspaint.exe \"" + path + "\"";
	else
		app ="C:\\Windows\\system32\\i_view64.exe \"" + path + "\"";
	if (CreateProcessA(NULL, const_cast<LPSTR>(app.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, &info, &processInfo))
	{
		WaitForSingleObject(processInfo.hProcess, INFINITE);
		CloseHandle(processInfo.hProcess);
		CloseHandle(processInfo.hThread);
	}
	DWORD errorMessageID = ::GetLastError();
	int ret = static_cast<int>(errorMessageID);
	return ret;
}