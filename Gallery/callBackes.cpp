#include <iostream>
#include <io.h>
#include <iostream>
#include <string>
#include "Album.h"
#include "User.h"
#include <map>

static int callback_getId(void* data, int argc, char** argv, char** azColName)
{
	*(int*)data = atoi(argv[0]);
	return 0;
}

static int callback_printTable(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " = " << argv[i] << " ";
	}
	std::cout << std::endl;
	return 0;
}

static int callback_getAlbumes(void* data, int argc, char** argv, char** azColName)
{
	//std::list<Album> *albumes = static_cast<std::list<Album> *>(data);
	std::string name;
	std::string creation_date;
	int userID;

	for (int i = 0; i < argc; i++)
	{
		//i == 0 is album id, not necessary
		if (i == 1)
			name = (std::string)argv[i];
		else if (i == 2)
			creation_date = (std::string)argv[i];
		else if (i == 3){
			userID = atoi(argv[i]);
			Album newAlbum(userID, name, creation_date);
			(*(std::list<Album> *)data).push_back(newAlbum);   //casting to list by value then push the album
		}
	}
	return 0;
}

static int callback_getAlbumesOf(void* data, int argc, char** argv, char** azColName)
{
	//std::list<Album> *albumes = static_cast<std::list<Album> *>(data);
	std::string name;
	std::string creation_date;
	int userID;
	int idToLookFor = 0;
	auto iterator = (*(std::list<Album> *)data).begin();
	// || (iterator->getName() == "null" && iterator->getCreationDate() == "null")
	if ((*(std::list<Album> *)data).size() >= 1) {
		idToLookFor = iterator->getOwnerId();
	}

	for (int i = 0; i < argc; i++)
	{
		//i == 0 is album id, not necessary
		if (i == 1)
			name = (std::string)argv[i];
		else if (i == 2)
			creation_date = (std::string)argv[i];
		else if (i == 3) {
			userID = atoi(argv[i]);
			if (userID == idToLookFor) {
				Album newAlbum(userID, name, creation_date);
				if (iterator->getName() == "null" && iterator->getCreationDate() == "null") {
					(*(std::list<Album> *)data).erase(iterator);
				}
				//(*(std::list<Album> *)data).erase(iterator);    //now delete the element who saved the ownerId
				(*(std::list<Album> *)data).push_back(newAlbum);   //casting to list by value then push the album
			}
		}
	}
	return 0;
}

static int callback_getUsers(void* data, int argc, char** argv, char** azColName)
{
	std::string name;
	int userID;
	int idToLookFor = 0;
	if ((std::string)(*(User*)data).getName() == "null") {
		idToLookFor = (*(User*)data).getId();
	}

	for (int i = 0; i < argc; i++)
	{
		if (i == 0)
			userID = atoi(argv[i]);
		else if (i == 1) {
			name = (std::string)argv[i];
			if (userID == idToLookFor) {
				(*(User*)data).setName(name);
			}
		}
	}
	return 0;
}

static int callback_albumExist (void* data, int argc, char** argv, char** azColName)
{
	std::string name;
	std::string creationDate = "";
	int userID;
	int idToLookFor = 0;
	std::string nameToLookFor = "";

	if ((std::string)(*(Album*)data).getCreationDate() == "null") {
		idToLookFor = (*(Album*)data).getOwnerId();
		nameToLookFor = (*(Album*)data).getName();
	}

	for (int i = 0; i < argc; i++)
	{
		if (i == 1)
			name = argv[i];
		else if (i == 1) {
			name = (std::string)argv[i];
		}
		else if (i == 2) {
			creationDate = (std::string)argv[i];
		}
		else if (i == 3) {
			userID = atoi(argv[i]);
			if (userID == idToLookFor && name == nameToLookFor) {
				(*(Album*)data).setCreationDate(creationDate);     //if creationDate not null so callback found this album
			}
		}
	}
	return 0;
}

static int callback_openAlbum(void* data, int argc, char** argv, char** azColName)
{
	std::string name;
	std::string creationDate = "";
	int userID;
	std::string nameToLookFor = "";

	if ((std::string)(*(Album*)data).getCreationDate() == "null") {
		nameToLookFor = (*(Album*)data).getName();
	}

	for (int i = 0; i < argc; i++)
	{
		if (i == 1)
			name = argv[i];
		else if (i == 2) {
			creationDate = (std::string)argv[i];
		}
		else if (i == 3) {
			userID = atoi(argv[i]);
			if (name == nameToLookFor) {    //found album, if not found so creationDate will be null and id -999
				(*(Album*)data).setOwner(userID);
				(*(Album*)data).setCreationDate(creationDate); 
			}
		}
	}
	return 0;
}


static int callback_getPictures(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* listPictures = reinterpret_cast<std::list<Picture>*>(data);
	Picture pic(0, "");
	int id;
	std::string name;
	std::string path;
	std::string creationDate;

	pic.setId((atoi)(argv[0]));
	pic.setName(argv[1]);
	pic.setPath(argv[2]);
	pic.setCreationDate(argv[3]);
	listPictures->push_back(pic);
	return 0;
}

static int callback_getTags(void* data, int argc, char** argv, char** azColName)
{
	std::set<int>* listTags = reinterpret_cast<std::set<int>*>(data);
	listTags->insert(atoi(argv[0]));
	return 0;
}

static int callback_lastId(void* data, int argc, char** argv, char** azColName)
{
	*(int*)data = atoi(argv[0]);
	return 0;
}

static int callback_coutUsers(void* data, int argc, char** argv, char** azColName)
{
	(*(int*)data)++;     //inc users found
	return 0;
}

static int callback_creatinDate_toAlbumId(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* listPictures = reinterpret_cast<std::list<Picture>*>(data);
	for (auto it = listPictures->begin(); it != listPictures->end(); it++) {
		if (it->getName() == argv[1])
			it->setCreationDate(argv[4]);     //save albumId in creationDate, creationDate is not necessary
	}
	return 0;
}

static int callback_getAlbumesStr(void* data, int argc, char** argv, char** azColName)
{
	std::set<std::string>* allAlbumsID = reinterpret_cast<std::set<std::string>*>(data);
	std::string idToAdd = std::string(argv[3]);
	allAlbumsID->insert(idToAdd);
	return 0;
}

static int callback_getUser(void* data, int argc, char** argv, char** azColName)
{
	(*(User*)data).setId(atoi(argv[2]));
	(*(User*)data).setName(argv[3]);
	return 0;
}

static int callback_getPic(void* data, int argc, char** argv, char** azColName)
{
	(*(Picture*)data).setId(atoi(argv[1]));
	(*(Picture*)data).setName(argv[4]);
	return 0;
}